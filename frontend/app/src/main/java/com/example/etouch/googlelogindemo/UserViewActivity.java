package com.example.etouch.googlelogindemo;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.DetectedActivity;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import java.util.HashMap;

import io.nlopez.smartlocation.OnActivityUpdatedListener;
import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.providers.LocationGooglePlayServicesProvider;

import static android.content.ContentValues.TAG;

public class UserViewActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnLocationUpdatedListener, OnActivityUpdatedListener {

    //code for location
    private LocationGooglePlayServicesProvider provider;
    private static final int LOCATION_PERMISSION_ID = 1001;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    HashMap user;
    String latValue, emailVal, longtiValue, userid,loginType;

    ProgressDialog dialog;
    TextView navUserMailText;
    //code for location
// Session Manager Class
    private FirebaseAuth mAuth;
    GoogleSignInClient mGoogleSignInClient;
    SessionManagement session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.setTitle("User Details");
        Intent intent = getIntent();
        // Session class instance
        CommonMethods.getCurrentTimeUsingDate();

        session = new SessionManagement(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        loginType = user.get(SessionManagement.KEY_LOGINTYPE);




        emailVal = user.get(SessionManagement.KEY_EMAIL);//intent.getStringExtra("emaiVal");
        userid = intent.getStringExtra("userid");


        String providergps = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if(!providergps.contains("gps")){
            displayLocationSettingsRequest(this);

        }

        if (savedInstanceState == null) {
            Fragment userViewFragment = new UserViewFragment();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(R.id.content_frame, userViewFragment);
            ft.addToBackStack(null);
            ft.commit();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header=navigationView.getHeaderView(0);
        /*View view=navigationView.inflateHeaderView(R.layout.nav_header_main);*/
        navUserMailText = (TextView)header.findViewById(R.id.navEmailValue);
        navUserMailText.setText(emailVal);
    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;
//        if (item.isChecked()){
//            item.setChecked(false);
//        }
        if (id == R.id.nav_Profile) {
            fragment = new UserViewFragment();
            // Handle the camera action
            // callLocationView();
            //startLocation();
        }
        if (id == R.id.nav_checkIn) {


            if (NetworkConnection.getInstance(this).isOnline()) {
                fragment = new MapsViewFragment();
               // callLocationView();
            } else {
                displaytoast(getString(R.string.offline_message));
            }
            //startLocation();
        }
        if (id == R.id.nav_orderDevice) {
            fragment = new DeviceorderFragment();
//            callOrderDeviceView();
//            session.logoutUser();
            // finish();
        }
        if (id == R.id.nav_myorders) {
            fragment = new OrdersListFragment();
//            ();
//            session.logoutUser();
            // finish();
        }
        if (id == R.id.nav_Logout) {
            userLogout();
//            session.logoutUser();
            // finish();
        }
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public void displaytoast(String toastmessage){
        Toast.makeText(this, toastmessage, Toast.LENGTH_LONG).show();
    }
    public void CallLocation() {
        if (ContextCompat.checkSelfPermission(UserViewActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(UserViewActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_ID);
            return;
        }
        // startLocation();


        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // showLast();
    }

    private void showLast() {
        Location lastLocation = SmartLocation.with(this).location().getLastLocation();
        if (lastLocation != null) {

            final String text1 = String.format("Latitude %.6f, Longitude %.6f",
                    lastLocation.getLatitude(),
                    lastLocation.getLongitude());
            Toast.makeText(this, "[From Cache]" + text1, Toast.LENGTH_LONG).show();
//            locationText.setText(
//                    String.format("[From Cache] Latitude %.6f, Longitude %.6f",
//                            lastLocation.getLatitude(),
//                            lastLocation.getLongitude())
//            );
        }

        DetectedActivity detectedActivity = SmartLocation.with(this).activity().getLastActivity();
        if (detectedActivity != null) {
//            activityText.setText(
//                    String.format("[From Cache] Activity %s with %d%% confidence",
//                            getNameFromType(detectedActivity),
//                            detectedActivity.getConfidence())
//            );
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (provider != null) {
            provider.onActivityResult(requestCode, resultCode, data);
        }
    }
    private void startLocation() {
        dialog = ProgressDialog.show(this, "Location", "Fetching Location...",
                true);
        dialog.show();
//        Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            public void run() {
//                dialog.show();
//            }
//        }, 4000); // 3000 milliseconds delay


        boolean gpsAvailable = SmartLocation.with(this).location().state().isGpsAvailable();
        Log.d(TAG, String.valueOf(gpsAvailable));
        if (!gpsAvailable) {
            displayLocationSettingsRequest(this);
        }
        provider = new LocationGooglePlayServicesProvider();
        provider.setCheckLocationSettings(true);

        SmartLocation smartLocation = new SmartLocation.Builder(this).logging(true).build();

        smartLocation.location(provider).start(this);
        smartLocation.activity().start(this);

        // Create some geofences
//        GeofenceModel mestalla = new GeofenceModel.Builder("1").setTransition(Geofence.GEOFENCE_TRANSITION_ENTER).setLatitude(39.47453120000001).setLongitude(-0.358065799999963).setRadius(500).build();
//        smartLocation.geofencing().add(mestalla).start(this);
    }

    @Override
    public void onLocationUpdated(Location location) {
        showLocation(location);
    }

    private void showLocation(Location location) {
        if (location != null) {
            final String text = String.format("Latitude %.6f, Longitude %.6f",
                    location.getLatitude(),
                    location.getLongitude());
            double latString = location.getLatitude();
            double longiString = location.getLongitude();
            latValue = Double.toString(latString);
            longtiValue = Double.toString(longiString);
            dialog.dismiss();
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            }
//            Intent locationView = new Intent(this, LocationView.class);
//            locationView.putExtra("latValue", latValue);
//            locationView.putExtra("LongtiValue", longtiValue);
//            startActivity(locationView);

            //locationText.setText(text);

            //   Toast.makeText(this,"normal"+text,Toast.LENGTH_LONG).show();
            // We are going to get the address for the current position
//            SmartLocation.with(this).geocoding().reverse(location, new OnReverseGeocodingListener() {
//                @Override
//                public void onAddressResolved(Location original, List<Address> results) {
//                    if (results.size() > 0) {
//                        Address result = results.get(0);
//                        StringBuilder builder = new StringBuilder(text);
//                        builder.append("\n[Reverse Geocoding] ");
//                        List<String> addressElements = new ArrayList<>();
//                        for (int i = 0; i <= result.getMaxAddressLineIndex(); i++) {
//                            addressElements.add(result.getAddressLine(i));
//                        }
//                        builder.append(TextUtils.join(", ", addressElements));
//                        locationText.setText(builder.toString());
//                    }
//                }
//            });
        } else {
            //  dialog.dismiss();
            Toast.makeText(this, "location not found", Toast.LENGTH_LONG).show();
            // locationText.setText("Null location");
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == LOCATION_PERMISSION_ID && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // startLocation();
        }
    }

    private void showActivity(DetectedActivity detectedActivity) {
        if (detectedActivity != null) {
//            activityText.setText(
//                    String.format("Activity %s with %d%% confidence",
//                            getNameFromType(detectedActivity),
//                            detectedActivity.getConfidence())
            // );
        } else {
            //activityText.setText("Null activity");
        }
    }

    @Override
    public void onActivityUpdated(DetectedActivity detectedActivity) {
        showActivity(detectedActivity);

    }

    private void displayLocationSettingsRequest(Context context) {
//        dialog = ProgressDialog.show(this, "Location", "Fetching Location...",
//                true);
//        dialog.show();

        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        Log.i(TAG, "All location settings are satisfied.");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(UserViewActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            Log.i(TAG, "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }

    @Override
    public void onPause() {

        super.onPause();
        if (dialog != null)
            dialog.dismiss();
    }

    public void userLogout() {
//        session = new SessionManagement(getApplicationContext());
//        HashMap<String, String> user = session.getUserDetails();
//        String loginType = user.get(SessionManagement.KEY_LOGINTYPE);
        if (loginType == "google") {
            GlobalState.mAuth.signOut();
            // Google sign out
            GlobalState.mGoogleSignInClient.signOut().addOnCompleteListener(this,
                    new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                        }
                    });
            session.logoutUser();
        } else {
            session.logoutUser();

        }
        finish();
    }
public void myordersView(){
//    Intent myOrdersActivity = new Intent(this, MyOrdersActivity.class);
//
//    startActivity(myOrdersActivity);
}

}
