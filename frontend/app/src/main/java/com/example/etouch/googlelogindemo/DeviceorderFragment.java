package com.example.etouch.googlelogindemo;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DeviceorderFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DeviceorderFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DeviceorderFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    SessionManagement session;
    private Button orderDeviceBtn;
    private  String userid,projectid;
    private EditText reqNoText,quantityText,deviceNameText,marketLinkText;
    ProgressDialog dialog;
    public HashMap resultData;
    ArrayList<ProjectsData> projectsDataList;
    Spinner spinner;
    private OnFragmentInteractionListener mListener;

    public DeviceorderFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DeviceorderFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DeviceorderFragment newInstance(String param1, String param2) {
        DeviceorderFragment fragment = new DeviceorderFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        reqNoText = (EditText) getView().findViewById(R.id.requestNo);
        quantityText = (EditText) getView().findViewById(R.id.quantityText);
        quantityText.setFilters(new InputFilter[]{ new MinMaxFilter("1", "5")});
        deviceNameText=(EditText)getView().findViewById(R.id.deviceName) ;
        marketLinkText = (EditText)getView().findViewById(R.id.marketLink) ;
        orderDeviceBtn = (Button) getView().findViewById(R.id.btn_order) ;
        session = new SessionManagement(getActivity().getApplicationContext());
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        HashMap<String, String> user = session.getUserDetails();
        userid  = user.get(SessionManagement.USER_ID);
        fetechProjectsData();
        resultData = new HashMap<String, String>();
        projectsDataList = new ArrayList<>();
        //projectNames = new ArrayList<String>();
        orderDeviceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendOrderDeviceData();
            }
        });
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_deviceorder, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("Device Order");

    }
    public void fetechProjectsData() {
        if (NetworkConnection.getInstance(getActivity()).isOnline()) {
            dialog = ProgressDialog.show(getActivity(), "Processing", "Please wait...",
                    true);
            dialog.show();//"^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$"//"^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{8,}$"
            final String URL = GlobalState.baseUrl + GlobalState.projectsApi;
            HashMap<String, String> params = new HashMap<String, String>();
            CommonMethods.sendDataToApi(URL, params, new VolleyResponseListener() {
                @Override
                public void onError(String message) {
                    dialog.dismiss();
                    displaytoast(message);
                }

                @Override
                public void onResponse(JSONObject response) {
                    String statuscode = null;
                    try {
                        statuscode = response.getString("status_code");
                        if (statuscode.equals("200")) {
                            dialog.dismiss();
                            //we have the array named projectsData inside the object
                            //so here we are getting that json array
                            JSONArray projectArray = response.getJSONArray("projects");
                            //projectnames1 = new String[projectArray.length()];
                            ProjectsData projectsData1 = new ProjectsData("Select ProjectName"," ", "");
                            projectsDataList.add(projectsData1);
                            for (int i = 0; i < projectArray.length(); i++) {
                                JSONObject jsonobject = projectArray.getJSONObject(i);
                                ProjectsData projectsData = new ProjectsData(jsonobject.getString("project_name"), jsonobject.getString("id"), jsonobject.getString("project_code"));
                                projectsDataList.add(projectsData);
                            }
                            spinnerInitilaize();
                            Log.v("Home", projectsDataList.toString());
                        } else {
                            displaytoast(response.getString("message"));
                            dialog.dismiss();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        dialog.dismiss();
                    }
                }
            });
//                    Toast.makeText(this, "Validation Successfull", Toast.LENGTH_LONG).show();
//
//
//                    Intent userViewActivity= new Intent(this, UserViewActivity.class);
//                    userViewActivity.putExtra("emaiVal", orgEmail.getText().toString());
//                    startActivity(userViewActivity);
//                    finish();
        } else {
            displaytoast(getString(R.string.offline_message));
        }
    }

    public void displaytoast(String toastmessage) {
        Toast.makeText(getActivity(), toastmessage, Toast.LENGTH_LONG).show();
    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();
        ProjectsData projectsData = (ProjectsData) parent.getItemAtPosition(position);
        projectid = projectsData.getProjectId();
        // Showing selected spinner item
        // Toast.makeText(this, "Selected: " + projectsData.getProjectName()+projectsData.getProjectId(), Toast.LENGTH_LONG).show();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
    public  void spinnerInitilaize(){
        spinner = (Spinner) getView().findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(this);

        ArrayAdapter<ProjectsData> adapter = new ArrayAdapter<ProjectsData>(getActivity(), android.R.layout.simple_spinner_dropdown_item, projectsDataList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);
    }
    public void sendOrderDeviceData(){

        if (projectid.equals("") ||projectid.equals(" ") || projectid.length() == 0 || quantityText.equals("") || quantityText.length() == 0 || deviceNameText.equals("") || deviceNameText.length() == 0 || marketLinkText.equals("") || marketLinkText.length() == 0) {
            Toast.makeText(getActivity(), "All fields are required", Toast.LENGTH_LONG).show();
            displaytoast("All fields are required.");
        }else{
            if (NetworkConnection.getInstance(getActivity()).isOnline()) {

                dialog = ProgressDialog.show(getActivity(), "Processing", "Please wait...",
                        true);
                dialog.show();//"^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$"//"^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{8,}$"
                final String URL = GlobalState.baseUrl+GlobalState.postorderdeviceApi;
                HashMap<String, String> params = new HashMap<String, String>();//
                params.put("userid", userid);
                params.put("project_id", projectid);
                params.put("srno",reqNoText.getText().toString());
                params.put("inventory_size",quantityText.getText().toString() );
                params.put("device_name", deviceNameText.getText().toString() );
                params.put("device_market_link", marketLinkText.getText().toString() );

                CommonMethods.sendDataToApi(URL,params, new VolleyResponseListener() {
                    @Override
                    public void onError(String message) {
                        dialog.dismiss();
                        displaytoast(message);
                    }
                    @Override
                    public void onResponse(JSONObject response) {
                        String statuscode = null;
                        try {
                            dialog.dismiss();
                            statuscode = response.getString("status_code");
                            if (statuscode.equals("200")){
                                displaytoast(response.getString("message"));
                                textFieldValuesNill();
                            }else{
                                displaytoast(response.getString("message"));
                                textFieldValuesNill();
                                dialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    }
                });
//                    Toast.makeText(this, "Validation Successfull", Toast.LENGTH_LONG).show();
//
//
//                    Intent userViewActivity= new Intent(this, UserViewActivity.class);
//                    userViewActivity.putExtra("emaiVal", orgEmail.getText().toString());
//                    startActivity(userViewActivity);
//                    finish();
            } else {
                displaytoast("You are not online!!!!");
//                Toast.makeText(this,"You are not online!!!!",Toast.LENGTH_LONG).show();
//                Log.v("Home", "############################You are not online!!!!");
            }
        }

    }
    public void textFieldValuesNill(){
        reqNoText.setText("");
        quantityText.setText("");
        deviceNameText.setText("");
        marketLinkText.setText("");
    }
}
