package com.example.etouch.googlelogindemo;

import java.io.Serializable;

public class MyOrdersList implements Serializable {
    String projectName;
    String deviceName;
    String srno;
    String userid;
    String marketLink;
    String inventorySize;
    String orderid;
    String orderStatus;
    String updateStatus;

    public MyOrdersList(String projectName, String deviceName, String srno,String userid, String marketLink, String inventorySize,String orderid,String orderStatus,String updateStatus) {
        this.projectName = projectName;
        this.deviceName = deviceName;
        this.srno = srno;
        this.userid = userid;
        this.marketLink = marketLink;
        this.inventorySize = inventorySize;
        this.orderid = orderid;
        this.orderStatus=orderStatus;
        this.updateStatus=updateStatus;
    }
    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getSrno() {
        return srno;
    }

    public void setSrno(String srno) {
        this.srno = srno;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getMarketLink() {
        return marketLink;
    }

    public void setMarketLink(String marketLink) {
        this.marketLink = marketLink;
    }

    public String getInventorySize() {
        return inventorySize;
    }

    public void setInventorySize(String inventorySize) {
        this.inventorySize = inventorySize;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }
    public String getUpdateStatus() {
        return updateStatus;
    }

    public void setUpdateStatus(String updateStatus) {
        this.updateStatus = updateStatus;
    }

}
