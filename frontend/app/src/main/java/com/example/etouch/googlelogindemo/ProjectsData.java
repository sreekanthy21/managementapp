package com.example.etouch.googlelogindemo;

public class ProjectsData {
    String projectName;
    String projectId;
    String ProjectCode;

    public ProjectsData(String projectName, String projectId, String ProjectCode) {
        this.projectName = projectName;
        this.projectId = projectId;
        this.ProjectCode = ProjectCode;
    }

    public String getProjectName() {
        return projectName;
    }
    public String getProjectId() {
        return projectId;
    }


    public String getProjectCode() {
        return ProjectCode;
    }

    /**
     * Pay attention here, you have to override the toString method as the
     * ArrayAdapter will reads the toString of the given object for the name
     *
     * @return contact_name
     */
    @Override
    public String toString() {
        return projectName;
    }

}
