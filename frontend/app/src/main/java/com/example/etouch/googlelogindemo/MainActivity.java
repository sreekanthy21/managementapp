package com.example.etouch.googlelogindemo;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button signIn;
    //private Button signOut;
    public static final int RC_SIGN_IN = 1;

    public static final String TAG = "MainActivity";

    protected static final int REQUEST_CHECK_SETTINGS = 0x1;
    SessionManagement session;
    boolean loginStatus;
    ProgressDialog dialog;
    public HashMap resultData;
    ///ui profile

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.setTitle("eTouch ATS");
        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // window.setStatusBarColor(this.getResources().getColor(R.color.example_color));
        session = new SessionManagement(getApplicationContext());
        loginStatus = session.isLoggedIn();
        resultData = new HashMap<String, String>();
        if (loginStatus) {
            // session.checkLogin();

            // get user data from session
            HashMap<String, String> user = session.getUserDetails();

            // name
            String name = user.get(SessionManagement.KEY_LOGINTYPE);

            // email
            String email = user.get(SessionManagement.KEY_EMAIL);
            Intent userViewActivity = new Intent(this, UserViewActivity.class);
            userViewActivity.putExtra("emaiVal", email);
            startActivity(userViewActivity);
            finish();
        } else {
            signIn = (Button) findViewById(R.id.sign_in_button);
            // signOut = (Button) findViewById(R.id.sign_out);
            GlobalState.mAuth = FirebaseAuth.getInstance();
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(getString(R.string.default_web_client_id))
                    .requestEmail()
                    .build();

            // Build a GoogleSignInClient with the options specified by gso.
            GlobalState.mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

            signIn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    signIn();
                }
            });

//            signOut.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    signOut();
//                }
//            });
            //findViewById(R.id.btn_register).setOnClickListener(this);
            findViewById(R.id.btn_login).setOnClickListener(this);
        }

    }

    private void signOut() {
        // Firebase sign out
        GlobalState.mAuth.signOut();

        // Google sign out
        GlobalState.mGoogleSignInClient.signOut().addOnCompleteListener(this,
                new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        updateUI(null);
                    }
                });
    }

    private void signIn() {
        if (NetworkConnection.getInstance(this).isOnline()) {
            Intent signInIntent = GlobalState.mGoogleSignInClient.getSignInIntent();
            startActivityForResult(signInIntent, RC_SIGN_IN);
            //Toast.makeText(this,"You are online!!!!",Toast.LENGTH_LONG).show();
        } else {
            displaytoast(getString(R.string.offline_message));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
                // ...
            }
        }
    }
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        GlobalState.mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = GlobalState.mAuth.getCurrentUser();
                            GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(getApplicationContext());
                            updateUI(acct);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(MainActivity.this, "you are not able to log in to google", Toast.LENGTH_LONG).show();
                            updateUI(null);
                        }
                    }
                });
    }

    private void updateUI(GoogleSignInAccount acct) {
        if (acct != null) {

            String personName = acct.getDisplayName();
            String personPhotoUrl = acct.getPhotoUrl().toString();
            String email = acct.getEmail();

            if (CommonMethods.checkEmailValidity(email)) {
                dialog = ProgressDialog.show(this, "Processing", "Please wait...",
                        true);
                dialog.show();//"^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$"//"^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{8,}$"
                final String URL = GlobalState.baseUrl + GlobalState.loginApi;
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("password", "");
                params.put("login_type", "sso");
                params.put("deviceid", CommonMethods.getAndroidID(this));

                CommonMethods.sendDataToApi(URL, params, new VolleyResponseListener() {
                    @Override
                    public void onError(String message) {
                        dialog.dismiss();
                        displaytoast(message);
                    }
                    @Override
                    public void onResponse(JSONObject response) {
                        String statuscode = null;
                        try {
                            statuscode = response.getString("status_code");
                            if (statuscode.equals("200")) {
                                resultData.put("message", response.getString("message"));
                                resultData.put("statuscode", response.getString("status_code"));
                                resultData.put("email", response.getString("email"));
                                resultData.put("userstatus", response.getString("userstatus"));
                                resultData.put("deviceid", response.getString("deviceid"));
                                resultData.put("userid", response.getString("userid"));
                                resultData.put("oldpassword", response.getString("password"));
                                if (response.getString("userstatus").equals("1")) {
                                    dialog.dismiss();
                                    //changePwdClass();
                                } else {
                                    dialog.dismiss();
                                    userViewClass();
                                }
                            } else {
                                displaytoast(response.getString("message"));
                                dialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    }
                });
            } else {
                final AlertDialog alertDialog = new AlertDialog.Builder(
                        MainActivity.this).create();

                // Setting Dialog Title
                alertDialog.setTitle("eTouchMS");

                // Setting Dialog Message
                alertDialog.setMessage("Please signin with your etouch mailid");

                // Setting Icon to Dialog
                // alertDialog.setIcon(R.drawable.tick);

                // Setting OK Button
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        signOut();
                        alertDialog.cancel();
                    }
                });

                // Showing Alert Message
                alertDialog.show();
            }

        } else {
            //   Toast.makeText(this, "Something went wrong..", Toast.LENGTH_SHORT).show();
//            signOut.setVisibility(View.GONE);
//            signIn.setVisibility(View.VISIBLE);
//            txtName.setText("");
//            txtEmail.setText("");
//            textTokenid.setText("");
//            Glide.with(getApplicationContext()).load("")
//                    .thumbnail(0.5f)
//                    .crossFade()
//                    .diskCacheStrategy(DiskCacheStrategy.ALL)
//                    .into(imgProfilePic);
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                Intent login = new Intent(this, OrgLoginActivity.class);
                startActivity(login);
                finish();
                break;
//            case R.id.btn_register:
//                Intent register= new Intent(this, RegisterActivity.class);
//                startActivity(register);
//                finish();
//                break;
        }
    }

    public void displaytoast(String toastmessage) {
        Toast.makeText(this, toastmessage, Toast.LENGTH_LONG).show();
    }

    public void userViewClass() {
        session.createLoginSession("google", resultData.get("email").toString(), resultData.get("userid").toString());
        GlobalState.googlelogin = "true";
        Intent userViewActivity = new Intent(this, UserViewActivity.class);
        userViewActivity.putExtra("emaiVal", resultData.get("email").toString());
        startActivity(userViewActivity);
        finish();
    }
}
