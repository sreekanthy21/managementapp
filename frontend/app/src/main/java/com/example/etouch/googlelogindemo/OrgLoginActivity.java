package com.example.etouch.googlelogindemo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.example.etouch.googlelogindemo.MainActivity.TAG;

public class OrgLoginActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText orgEmail,orgPwd;
    private Button btnLogin;
    SessionManagement session;
    private AwesomeValidation awesomeValidation;
    boolean loginStatus;
    public HashMap resultData;
    ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_org_login);
        this.setTitle("eTouch ATS");
        //initializing view objects
        session = new SessionManagement(getApplicationContext());
        orgEmail = (EditText) findViewById(R.id.orgEmailtext);
        orgPwd = (EditText) findViewById(R.id.orgPwdText);
        session = new SessionManagement(getApplicationContext());
        findViewById(R.id.btn_OrgLogin).setOnClickListener(this);
        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
//        Toast.makeText(getApplicationContext(), "User Login Status: " + session.isLoggedIn(), Toast.LENGTH_LONG).show();
        loginStatus = session.isLoggedIn();
        resultData = new HashMap<String, String>();
    }
    public boolean checkEmailValidity(){

        String getEmail = orgEmail.getText().toString();
        boolean getEnd;

        //CHECK STARTING STRING IF THE USER
        //entered @gmail.com / @yahoo.com / @outlook.com only
        boolean getResult = !TextUtils.isEmpty(getEmail) && android.util.Patterns.EMAIL_ADDRESS.matcher(getEmail).matches();

        //CHECK THE EMAIL EXTENSION IF IT ENDS CORRECTLY
        if (getEmail.endsWith("@virtusa.com")){
            getEnd = true;
        }else {
            getEnd = false;

        }

        //TEST THE START AND END
        return (getResult && getEnd);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_OrgLogin:
                loginBtnTapped();
                break;

        }
    }
    private void loginBtnTapped() {
       String domainmail ="virtusa.com";
        if (checkEmailValidity())
        {
            //awesomeValidation.addValidation(this, R.id.orgPwdText, "^(?=.*[0-9])(?=.*[a-z])(?=.*[!@#$%^&*+=?-]).{8,15}$", R.string.passworderror);
//            if(awesomeValidation.validate()){
                if (NetworkConnection.getInstance(this).isOnline()) {
                    dialog = ProgressDialog.show(this, "Processing", "Please wait...",
                            true);
                    dialog.show();//"^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$"//"^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{8,}$"
                    final String URL = GlobalState.baseUrl+GlobalState.loginApi;
                    HashMap<String, String> params = new HashMap<String, String>();
                    params.put("email", orgEmail.getText().toString());
                    params.put("password", orgPwd.getText().toString());
                    params.put("login_type", "virtusa");
                    params.put("deviceid", CommonMethods.getAndroidID(this));

                    CommonMethods.sendDataToApi(URL,params, new VolleyResponseListener() {
                        @Override
                        public void onError(String message) {
                            dialog.dismiss();
                            displaytoast(message);
                        }
                        @Override
                        public void onResponse(JSONObject response) {
                            String statuscode = null;
                            try {
                                statuscode = response.getString("status_code");
                                if (statuscode.equals("200")){
                                    resultData.put("message",response.getString("message"));
                                    resultData.put("statuscode",response.getString("status_code"));
                                    resultData.put("email",response.getString("email"));
                                    resultData.put("userstatus",response.getString("userstatus"));
                                    resultData.put("deviceid",response.getString("deviceid"));
                                    resultData.put("userid",response.getString("userid"));
                                    resultData.put("oldpassword",response.getString("password"));
                                    if (response.getString("userstatus").equals("1")){
                                        dialog.dismiss();
                                        changePwdClass();
                                    }else {
                                        dialog.dismiss();
                                        userViewClass();
                                    }
                                }else{
                                    displaytoast(response.getString("message"));
                                    dialog.dismiss();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                dialog.dismiss();
                            }
                        }
                    });

                } else {
                    displaytoast(getString(R.string.offline_message));
                }

 //           }
        }else {
            orgEmail.setError("Please Enter virtusa Email Address");
        }
    }
    public void changePwdClass(){
        Log.d(TAG, "onlogin: " + resultData);
        Intent changePasswordActivity= new Intent(this, ChangePasswordActivity.class);
        changePasswordActivity.putExtra("emaiVal", resultData.get("email").toString());
        changePasswordActivity.putExtra("oldpwd", resultData.get("oldpassword").toString());
        startActivity(changePasswordActivity);
        finish();
    }
    public void displaytoast(String toastmessage){
        Toast.makeText(this, toastmessage, Toast.LENGTH_LONG).show();
    }
    public void userViewClass(){

        session.createLoginSession("org", resultData.get("email").toString(),resultData.get("userid").toString());
        Intent userViewActivity= new Intent(this, UserViewActivity.class);
        userViewActivity.putExtra("emaiVal", resultData.get("email").toString());
        userViewActivity.putExtra("userid", resultData.get("userid").toString());
        startActivity(userViewActivity);
        finish();
    }
    @Override
    public void onPause(){

        super.onPause();
        if(dialog != null)
            dialog.dismiss();
    }
}
