package com.example.etouch.googlelogindemo;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MapsViewFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MapsViewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MapsViewFragment extends Fragment implements OnMapReadyCallback, View.OnClickListener{

    private GoogleMap mMap;
    private static final String TAG = "MapsActivity";
    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private static final float DEFAULT_ZOOM = 15f;
    ProgressDialog dialog;
    public String latValue, emailVal, longtiValue,userid;
    private boolean timeInterval;
    private Button locationBtn;
    //vars
    private Boolean mLocationPermissionsGranted = false;
    SessionManagement session;
    private FusedLocationProviderClient mFusedLocationProviderClient;

    private OnFragmentInteractionListener mListener;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        timeInterval = CommonMethods.isTimeWith_in_Interval();
        locationBtn = (Button) getView().findViewById(R.id.butnLocationSave);
        getView().findViewById(R.id.butnLocationSave).setOnClickListener(this);
        if (timeInterval) {
            locationBtn.setEnabled(true);
        }else {
            locationBtn.setAlpha(.5f);
            locationBtn.setEnabled(false);
        }


        session = new SessionManagement(getActivity().getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        userid  = user.get(SessionManagement.USER_ID);
        emailVal = user.get(SessionManagement.KEY_EMAIL);

        getLocationPermission();
    }

    public MapsViewFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MapsViewFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MapsViewFragment newInstance(String param1, String param2) {
        MapsViewFragment fragment = new MapsViewFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_maps_view, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("Location View");

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }
    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // dialog = ProgressDialog.show(this, "Location", "Fetching Location...",
        //         true);
        //  dialog.show();
        // Add a marker in Sydney and move the camera
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        if (mLocationPermissionsGranted) {
            getDeviceLocation();
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);

        }

    }

    private void getDeviceLocation() {
        Log.d(TAG, "getDeviceLocation: getting the devices current location");

        try {
            if (mLocationPermissionsGranted) {

                final Task location = mFusedLocationProviderClient.getLastLocation();
                location.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
//                        dialog.dismiss();
                        if (task.isSuccessful() && task.getResult() != null) {
                            Log.d(TAG, "onComplete: found location!");
                            Location currentLocation = (Location) task.getResult();
                            latValue = (Double.toString(currentLocation.getLatitude()));
                            longtiValue = (Double.toString(currentLocation.getLongitude()));
                            LatLng sydney = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
                            mMap.addMarker(new MarkerOptions().position(sydney).title(sydney.toString()));

                            moveCamera(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()),
                                    DEFAULT_ZOOM);

                        } else {
                            Log.d(TAG, "onComplete: current location is null");
                            Toast.makeText(getActivity(), "unable to get current location", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        } catch (SecurityException e) {
            Log.e(TAG, "getDeviceLocation: SecurityException: " + e.getMessage());
        }
    }

    private void moveCamera(LatLng latLng, float zoom) {
        Log.d(TAG, "moveCamera: moving the camera to: lat: " + latLng.latitude + ", lng: " + latLng.longitude);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
    }

    private void initMap() {
        Log.d(TAG, "initMap: initializing map");
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
    }

    private void getLocationPermission() {
        Log.d(TAG, "getLocationPermission: getting location permissions");
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION};

        if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
                FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
                    COURSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mLocationPermissionsGranted = true;
                initMap();
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        permissions,
                        LOCATION_PERMISSION_REQUEST_CODE);
            }
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    permissions,
                    LOCATION_PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult: called.");
        mLocationPermissionsGranted = false;

        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            mLocationPermissionsGranted = false;
                            Log.d(TAG, "onRequestPermissionsResult: permission failed");
                            return;
                        }
                    }
                    Log.d(TAG, "onRequestPermissionsResult: permission granted");
                    mLocationPermissionsGranted = true;
                    initMap();
                }
            }
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.butnLocationSave:
                saveLocationDetails();
                //Toast.makeText(MapsActivity.this, "Location details saved", Toast.LENGTH_SHORT).show();
                break;

        }
    }


    public void saveLocationDetails() {
        if (latValue.equals("") || latValue.length() == 0 || longtiValue.equals("") || longtiValue.length() == 0) {
            Toast.makeText(getActivity(), "Please refresh your View !!!!", Toast.LENGTH_LONG).show();
        } else {
            if (NetworkConnection.getInstance(getActivity()).isOnline()) {
                dialog = ProgressDialog.show(getActivity(), "Processing", "Please wait...",
                        true);
                dialog.show();
                //"^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$"//"^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{8,}$"
                final String URL = GlobalState.baseUrl + GlobalState.locationApi;
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("email", emailVal);
                params.put("lattitude", latValue);
                params.put("longitude", longtiValue);
                params.put("userid",userid);
                params.put("deviceid", CommonMethods.getAndroidID(getActivity()));

                CommonMethods.sendDataToApi(URL, params, new VolleyResponseListener() {
                    @Override
                    public void onError(String message) {
                        displaytoast(message);
                        dialog.dismiss();
                    }

                    @Override
                    public void onResponse(JSONObject response) {
                        String statuscode = null;
                        try {
                            statuscode = response.getString("status_code");
                            if (statuscode.equals("200")) {
                                dialog.dismiss();
                                displaytoast(response.getString("message"));
                                //          orgLoginView();
//                                if (response.getString("userstatus").equals("1")) {
//                                    changePwdClass();
//                                    resultData.put("message", response.getString("message"));
//                                    resultData.put("statuscode", response.getString("status_code"));
//                                    resultData.put("email", response.getString("email"));
//                                    resultData.put("userstatus", response.getString("userstatus"));
//                                    resultData.put("deviceid", response.getString("deviceid"));
//                                    resultData.put("oldpassword", response.getString("oldpassword"));
//                                } else {
//                                    userViewClass();
//                                }
                            } else {
                                displaytoast(response.getString("message"));
                                dialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    }
                });
            } else {
                displaytoast(getString(R.string.offline_message));
            }
        }
    }

    public void displaytoast(String toastmessage) {
        Toast.makeText(getActivity(), toastmessage, Toast.LENGTH_LONG).show();
    }
    @Override
    public void onPause(){

        super.onPause();
        if(dialog != null)
            dialog.dismiss();
    }
}
