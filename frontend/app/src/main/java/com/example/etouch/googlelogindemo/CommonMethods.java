package com.example.etouch.googlelogindemo;

import android.content.Context;
import android.location.LocationManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import static com.example.etouch.googlelogindemo.MainActivity.TAG;

public class CommonMethods {

    public static String getAndroidID(Context context) {
        String android_id = Settings.System.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        return android_id;
    }

    public static boolean getGpsStatus(Context context) {
        boolean gpsStatus;
        final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            gpsStatus = false;
        } else {
            gpsStatus = true;
        }
        return gpsStatus;
    }

    public static void getCurrentTimeUsingDate() {
        Date date = new Date();
        String strDateFormat = "hh:mm:ss a";
        DateFormat dateFormat = new SimpleDateFormat(strDateFormat);
        String formattedDate = dateFormat.format(date);
        Log.d(TAG, "Current time of the day using Date - 12 hour format:: " + formattedDate);

    }

    public static boolean isTimeWith_in_Interval() {

        boolean isBetween = false;
        try {
            Date date = new Date();
            String strDateFormat = "hh:mm:ss a";
            DateFormat dateFormat = new SimpleDateFormat(strDateFormat);
            String formattedDate = dateFormat.format(date);
            Date time1 = new SimpleDateFormat("hh:mm:ss a").parse(GlobalState.endTime);

            Date time2 = new SimpleDateFormat("hh:mm:ss a").parse(GlobalState.startTime);

            Date d = new SimpleDateFormat("hh:mm:ss a").parse(formattedDate);

            if (time1.after(d) && time2.before(d)) {
                isBetween = true;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "isBetween:: " + isBetween);
        return isBetween;
    }

    public static void getCurrentTimeUsingCalendar() {
        Calendar cal = Calendar.getInstance();
        Date date = cal.getTime();
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        String formattedDate = dateFormat.format(date);
        System.out.println("Current time of the day using Calendar - 24 hour format: " + formattedDate);
    }

    public static void sendDataToApi(String url, HashMap params, final VolleyResponseListener listener) {

        JsonObjectRequest request_json = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //Process os success response
                        try {
                            VolleyLog.v("Response:%n %s", response.toString(4));
                            listener.onResponse(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                listener.onError("Server Not Available");
                VolleyLog.e("Error: ", "Server Not Available");
            }
        });

        // add the request object to the queue to be executed
        ApplicationController.getInstance().addToRequestQueue(request_json);
    }

    public static boolean checkEmailValidity(String emailid) {

        boolean getEnd;

        //CHECK STARTING STRING IF THE USER
        //entered @gmail.com / @yahoo.com / @outlook.com only
        boolean getResult = !TextUtils.isEmpty(emailid) && android.util.Patterns.EMAIL_ADDRESS.matcher(emailid).matches();

        //CHECK THE EMAIL EXTENSION IF IT ENDS CORRECTLY
        if (emailid.endsWith("@etouch.net")) {
            getEnd = true;
        } else {
            getEnd = false;

        }
        //TEST THE START AND END
        return (getResult && getEnd);
    }


}
