package com.example.etouch.googlelogindemo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class MYOrderDetailsActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    private TextView detailreqNo,detailquantity,detaildeviceName,detailmarketLink,detailsProjectName;
    private Spinner spinner2;
    String statusText;
    ProgressDialog dialog;
    private Button detailsUpdateBtn;
   String[] statusList ;
    MyOrdersList myOrdersDetailData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myorder_details);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        myOrdersDetailData= (MyOrdersList) getIntent().getSerializableExtra("listValueDetails");
        Intent intent = getIntent();
        statusList = intent.getStringArrayExtra("orderStatus");
        detailreqNo = (TextView) findViewById(R.id.details_RequestNo);
        detailquantity = (TextView) findViewById(R.id.details_Quantity);
        detaildeviceName = (TextView) findViewById(R.id.details_DeviceName);
        detailmarketLink = (TextView) findViewById(R.id.details_MarketLink);
        detailsProjectName = (TextView) findViewById(R.id.details_ProjectName);
        detailsUpdateBtn = (Button) findViewById(R.id.btn_UpdateOrder) ;

        detailsProjectName.setText(myOrdersDetailData.getProjectName());
        detailquantity.setText(myOrdersDetailData.getInventorySize());
        detaildeviceName.setText(myOrdersDetailData.getDeviceName());
        detailmarketLink.setText(myOrdersDetailData.getMarketLink());
        detailreqNo.setText(myOrdersDetailData.getSrno());

        int i = Arrays.asList(statusList).indexOf(myOrdersDetailData.getOrderStatus());
        spinner2 = (Spinner)findViewById(R.id.statusSpinner);
        if (myOrdersDetailData.getOrderStatus().equalsIgnoreCase("In Hand")){
            spinner2.setEnabled(false);
            spinner2.setClickable(false);
            detailsUpdateBtn.setVisibility(View.GONE);
        }else{
            spinner2.setEnabled(true);
            spinner2.setClickable(true);
            detailsUpdateBtn.setVisibility(View.VISIBLE);

       }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MYOrderDetailsActivity.this,
                android.R.layout.simple_spinner_item,statusList);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter);
        spinner2.setSelection(i);
        spinner2.setOnItemSelectedListener(this);
        Log.v("Home", myOrdersDetailData.getDeviceName());
        detailsUpdateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendOrderUpdateData();
            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
         statusText = parent.getItemAtPosition(position).toString();
//        int i = Arrays.asList(statusList).indexOf(myOrdersDetailData.getOrderStatus());
//         spinner2.setSelection(i);
        // Showing selected spinner item
         //Toast.makeText(this, "Selected: " + statusText, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    public void sendOrderUpdateData(){

        if (statusText.equals("") ||statusText.equals(" ") || statusText.length() == 0 ) {
            displaytoast("Please select status.");
        }else{
            if (NetworkConnection.getInstance(this).isOnline()) {

                dialog = ProgressDialog.show(this, "Processing", "Please wait...",
                        true);
                dialog.show();//"^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$"//"^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{8,}$"
                final String URL = GlobalState.baseUrl+GlobalState.postUpdateorderApi;
                HashMap<String, String> params = new HashMap<String, String>();//
                params.put("orderid",myOrdersDetailData.getOrderid() );
                params.put("status", statusText);

                CommonMethods.sendDataToApi(URL,params, new VolleyResponseListener() {
                    @Override
                    public void onError(String message) {
                        dialog.dismiss();
                        displaytoast(message);
                    }
                    @Override
                    public void onResponse(JSONObject response) {
                        String statuscode = null;
                        try {
                            dialog.dismiss();
                            statuscode = response.getString("status_code");
                            if (statuscode.equals("200")){
                                displaytoast(response.getString("message"));
                            }else{
                                displaytoast(response.getString("message"));
                                dialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    }
                });

            } else {
                displaytoast("You are not online!!!!");

            }
        }
    }
    public void displaytoast(String toastmessage){
        Toast.makeText(this, toastmessage, Toast.LENGTH_LONG).show();
    }
}
