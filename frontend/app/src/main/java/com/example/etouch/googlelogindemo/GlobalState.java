package com.example.etouch.googlelogindemo;

import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.firebase.auth.FirebaseAuth;

public class GlobalState {
    private static final GlobalState ourInstance = new GlobalState();

    public static GlobalState getInstance() {
        return ourInstance;
    }

    private GlobalState() {
    }
    public static GoogleSignInClient mGoogleSignInClient;
    public static FirebaseAuth mAuth;
    public static String googlelogin = "false";

    public static  String  startTime="08:00:00 AM";
    public static  String  endTime="12:00:00 PM";

    public static  String baseUrl="http://d7070860.ngrok.io/";
    public static  String loginApi="login/";
    public static  String changePasswordApi="changepassword/";
    public static  String  locationApi="checkin/";
    public static  String  projectsApi="projects/";
    public static  String  postorderdeviceApi="postorder/";
    public static  String  postmyordersApi="myorders/?userid=";
    public static  String  postUpdateorderApi="updateorder/";

}
