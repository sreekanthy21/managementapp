package com.example.etouch.googlelogindemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.regex.Pattern;

public class RegisterActivity extends AppCompatActivity implements  View.OnClickListener{
    private EditText firstName,lastName ,emailText,pwdText,mobileNum;
    private Button btnSubmit;

    private AwesomeValidation awesomeValidation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        this.setTitle("Registration Page");

        //initializing view objects
        firstName = (EditText) findViewById(R.id.firstName);
        lastName = (EditText) findViewById(R.id.lastName);
        emailText = (EditText) findViewById(R.id.emailText);
        pwdText = (EditText) findViewById(R.id.passwordtext);
        mobileNum = (EditText) findViewById(R.id.mobileNum);

        findViewById(R.id.buttonRegister).setOnClickListener(this);

        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);

        //adding validation to edittexts


    }
    //CHECK EMAIL
    public boolean checkEmailValidity(){

        String getEmail = emailText.getText().toString();
        boolean getEnd;

        //CHECK STARTING STRING IF THE USER
        //entered @gmail.com / @yahoo.com / @outlook.com only
        boolean getResult = !TextUtils.isEmpty(getEmail) && android.util.Patterns.EMAIL_ADDRESS.matcher(getEmail).matches();

        //CHECK THE EMAIL EXTENSION IF IT ENDS CORRECTLY
        if (getEmail.endsWith("@virtusa.com")){
            getEnd = true;
        }else {
            getEnd = false;

        }

        //TEST THE START AND END
        return (getResult && getEnd);
    }
    private void submitForm() {
        //first validate the form then move ahead
        //if this becomes true that means validation is successfull
        awesomeValidation.addValidation(this, R.id.firstName, "^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$", R.string.firstnameerror);
        awesomeValidation.addValidation(this, R.id.lastName, "^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$", R.string.lastnameerror);
        if (awesomeValidation.validate() ){
            //Toast.makeText(this, "Validation Successfull", Toast.LENGTH_LONG).show();
            emailValidation();
            //process the data further
        }
    }
    private void emailValidation() {
        if(CommonMethods.checkEmailValidity(emailText.getText().toString())){

            awesomeValidation.addValidation(this, R.id.passwordtext, "^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$", R.string.passworderror);
            awesomeValidation.addValidation(this, R.id.mobileNum, "^[2-9]{2}[0-9]{8}$", R.string.mobileerror);
            if (awesomeValidation.validate() ){
               // Toast.makeText(this, "Validation Successfull", Toast.LENGTH_LONG).show();
                //process the data further
                //sendDataToApi();
                Intent orgLoginActivity= new Intent(this, OrgLoginActivity.class);
                startActivity(orgLoginActivity);
                finish();
            }
        }else{
            emailText.setError("Please Enter virtusa Email Address");
            //Toast.makeText(this, "Please enter valid email", Toast.LENGTH_LONG).show();
        }


    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonRegister:
               // sendDataToApi();
                submitForm();
                break;

        }
    }
    public void sendDataToApi(){
        final String URL = "http://100.71.224.21:8000/register/";
// Post params to be sent to the server
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("first_name", "santosh");
        params.put("last_name", "reddy");
        params.put("email", "santoshr@virtusa.com");
        params.put("password", "etouch@123");
        params.put("contact", "9022909876");

        JsonObjectRequest request_json = new JsonObjectRequest(Request.Method.POST,URL, new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //Process os success response
                        try {
                            VolleyLog.v("Response:%n %s", response.toString(4));
                            //now get your  json array like this
                           // JSONArray booking = response.getJSONArray("savebooking");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.e("Error: ", error.getMessage());
            }
        });

         // add the request object to the queue to be executed
        ApplicationController.getInstance().addToRequestQueue(request_json);
    }
}
