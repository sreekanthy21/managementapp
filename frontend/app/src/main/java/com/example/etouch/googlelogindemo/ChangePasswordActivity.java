package com.example.etouch.googlelogindemo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.example.etouch.googlelogindemo.MainActivity.TAG;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView oldPassword, newPassword, confirmPassword;
    public String oldPwd, emailValue;
    public HashMap resultData;
    ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        this.setTitle("Update Password");
        Intent intent = getIntent();
        emailValue = intent.getStringExtra("emaiVal");
        oldPwd = intent.getStringExtra("oldpwd");
        Log.d(TAG, "changefirst: " + oldPwd);
        oldPassword = (EditText) findViewById(R.id.oldPwdText);
        newPassword = (EditText) findViewById(R.id.newPwdText);
        confirmPassword = (EditText) findViewById(R.id.confirmPwdText);

        findViewById(R.id.butnLocationSave).setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.butnLocationSave:
                SaveDetails();
                break;
        }
    }

    public void SaveDetails() {

        String getPassword = oldPassword.getText().toString();
        String getNewPassword = newPassword.getText().toString();
        String getConfirmPassword = confirmPassword.getText().toString();

        if (getPassword.equals("") || getPassword.length() == 0 || getNewPassword.equals("") || getNewPassword.length() == 0 || getConfirmPassword.equals("") || getConfirmPassword.length() == 0) {
            Toast.makeText(ChangePasswordActivity.this, "Password fields should not be empty", Toast.LENGTH_LONG).show();
        } else if (!getPassword.equals(oldPwd)) {
            Toast.makeText(ChangePasswordActivity.this, "Old password doesn't match.", Toast.LENGTH_LONG).show();
        } else if (!getNewPassword.equals(getConfirmPassword)) {
            Toast.makeText(ChangePasswordActivity.this, "New and confirm password doesn't match.", Toast.LENGTH_LONG).show();
        } else {
           // Toast.makeText(ChangePasswordActivity.this, "Submitted data successfully", Toast.LENGTH_LONG).show();
            if (NetworkConnection.getInstance(this).isOnline()) {
                dialog = ProgressDialog.show(this, "Processing", "Please wait...",
                        true);
                dialog.show();
                //"^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$"//"^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{8,}$"
                final String URL = GlobalState.baseUrl + GlobalState.changePasswordApi;
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("email",emailValue);
                params.put("oldpassword", oldPwd);
                params.put("newpassword", getNewPassword);

                CommonMethods.sendDataToApi(URL, params, new VolleyResponseListener() {
                    @Override
                    public void onError(String message) {
                        displaytoast(message);
                        dialog.dismiss();
                    }

                    @Override
                    public void onResponse(JSONObject response) {
                        String statuscode = null;
                        try {
                            statuscode = response.getString("status_code");
                            if (statuscode.equals("200")) {
                                displaytoast(response.getString("message"));
                                orgLoginView();
//                                if (response.getString("userstatus").equals("1")) {
//                                    changePwdClass();
//                                    resultData.put("message", response.getString("message"));
//                                    resultData.put("statuscode", response.getString("status_code"));
//                                    resultData.put("email", response.getString("email"));
//                                    resultData.put("userstatus", response.getString("userstatus"));
//                                    resultData.put("deviceid", response.getString("deviceid"));
//                                    resultData.put("oldpassword", response.getString("oldpassword"));
//                                } else {
//                                    userViewClass();
//                                }
                            } else {
                                displaytoast(response.getString("message"));
                                dialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    }
                });
            } else {
                displaytoast(getString(R.string.offline_message));
            }
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
    public void displaytoast(String toastmessage){
        Toast.makeText(this, toastmessage, Toast.LENGTH_LONG).show();
    }
    public void orgLoginView(){
        Intent orgLoginActivity = new Intent(this, OrgLoginActivity.class);
        startActivity(orgLoginActivity);
        finish();
    }
    @Override
    public void onPause(){

        super.onPause();
        if(dialog != null)
            dialog.dismiss();
    }
}
