package com.example.etouch.googlelogindemo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomAdapter extends ArrayAdapter<MyOrdersList> {
    private ArrayList<MyOrdersList> dataSet;
    Context mContext;
    private int lastPosition = -1;
    // View lookup cache
    private static class ViewHolder {
  //      TextView txtProjectName;
        TextView txtQuantity;
        TextView txtdevice;
        TextView txtRequstNo;
        TextView txtStatus;

        ImageView arrowImage;
    }

    public CustomAdapter(ArrayList<MyOrdersList> data, Context context) {
        super(context, R.layout.row_item, data);
        this.dataSet = data;
        this.mContext=context;

    }


//    @Override
//    public void onClick(View v) {
//
//
//        int position=(Integer) v.getTag();
//        Object object= getItem(position);
//        DataModel dataModel=(DataModel)object;
//
//
//
//
//        switch (v.getId())
//        {
//
//            case R.id.item_info:
//
//                Snackbar.make(v, "Release date " +dataModel.getFeature(), Snackbar.LENGTH_LONG)
//                        .setAction("No action", null).show();
//
//                break;
//
//
//        }
//
//
//    }
//
//    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        MyOrdersList myOrdersList = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {


            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.row_item, parent, false);
       //     viewHolder.txtProjectName = (TextView) convertView.findViewById(R.id.ord_ProjectName);
            viewHolder.txtQuantity = (TextView) convertView.findViewById(R.id.ord_Quantity);
            viewHolder.txtdevice = (TextView) convertView.findViewById(R.id.ord_DeviceName);
            viewHolder.txtRequstNo = (TextView) convertView.findViewById(R.id.ord_RequestNo);
            viewHolder.txtStatus = (TextView) convertView.findViewById(R.id.ord_Status);
         //  viewHolder.arrowImage = (ImageView) convertView.findViewById(R.id.ord);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;


        //viewHolder.txtProjectName.setText(myOrdersList.getProjectName());
        viewHolder.txtQuantity.setText(myOrdersList.getInventorySize());
        viewHolder.txtdevice.setText(myOrdersList.getDeviceName());
        viewHolder.txtRequstNo.setText(myOrdersList.getSrno());
        viewHolder.txtStatus.setText(myOrdersList.getOrderStatus());
        //viewHolder.info.setOnClickListener(this);
     //   viewHolder.info.setTag(position);
        // Return the completed view to render on screen
        return convertView;
    }
}
