package com.example.etouch.googlelogindemo;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OrdersListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OrdersListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OrdersListFragment extends Fragment {

    ProgressDialog dialog;
    ArrayList<MyOrdersList> myordersList;
    SessionManagement session;
    String userid;
    ListView listView;
    String[] statusList;
    private static CustomAdapter adapter;
    private OnFragmentInteractionListener mListener;


    public OrdersListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OrdersListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OrdersListFragment newInstance(String param1, String param2) {
        OrdersListFragment fragment = new OrdersListFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        session = new SessionManagement(getActivity().getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        userid = user.get(SessionManagement.USER_ID);
        myordersList = new ArrayList<>();

    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_orders_list, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle("Orders List");
        fetechMyOrdersData();

    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    public void fetechMyOrdersData() {
        if (NetworkConnection.getInstance(getActivity().getApplicationContext()).isOnline()) {
            dialog = ProgressDialog.show(getActivity(), "Processing", "Please wait...",
                    true);
            dialog.show();//"^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$"//"^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{8,}$"
            final String URL = GlobalState.baseUrl + GlobalState.postmyordersApi + userid;
            HashMap<String, String> params = new HashMap<String, String>();
            CommonMethods.sendDataToApi(URL, params, new VolleyResponseListener() {
                @Override
                public void onError(String message) {
                    dialog.dismiss();
                    displaytoast(message);
                }

                @Override
                public void onResponse(JSONObject response) {
                    String statuscode = null;

                    try {
                        statuscode = response.getString("status_code");
                        if (statuscode.equals("200")) {
                            dialog.dismiss();
                            //we have the array named projectsData inside the object
                            //so here we are getting that json array

                            JSONArray myordersArray = response.getJSONArray("orders");
                            JSONArray orderStatusArray = response.getJSONArray("order_status");
                            statusList = new String[orderStatusArray.length()];
                            for (int i = 0; i < orderStatusArray.length(); i++) {

                                String value = orderStatusArray.getString(i);
                                statusList[i]=value;
                                //adding the projectsData to projectsDataList
                                //projectnames1[i] = jsonobject.getString("project_name");

                            }
                            Log.v("statusList", statusList.toString());
                            myordersList.clear();
                            for (int i = 0; i < myordersArray.length(); i++) {
                                JSONObject jsonobject = myordersArray.getJSONObject(i);
                                MyOrdersList  myOrdersList = new MyOrdersList(jsonobject.getString("project_name"), jsonobject.getString("device_name"), jsonobject.getString("srno"), jsonobject.getString("userid"), jsonobject.getString("device_market_link"), jsonobject.getString("inventory_size"), jsonobject.getString("id"),jsonobject.getString("status"),jsonobject.getString("update_status"));
                                //adding the projectsData to projectsDataList
                                //projectnames1[i] = jsonobject.getString("project_name");
                                myordersList.add(myOrdersList);
                            }
                            listViewIntilize();
                            Log.v("Home", myordersList.toString());
                        } else {
                            displaytoast(response.getString("message"));
                            dialog.dismiss();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        dialog.dismiss();
                    }
                }
            });

        } else {
            displaytoast(getString(R.string.offline_message));
        }
    }

    public void displaytoast(String toastmessage) {
        Toast.makeText(getActivity(), toastmessage, Toast.LENGTH_LONG).show();
    }

    public void listViewIntilize() {
        listView = (ListView) getView().findViewById(R.id.list);
        adapter = new CustomAdapter(myordersList, getActivity().getApplicationContext());
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                MyOrdersList myOrdersList = myordersList.get(position);
                callmyOrdetailsView(myOrdersList);
//                Snackbar.make(view, myOrdersList.getProjectName() + "\n" + myOrdersList.getInventorySize() + " API: " + myOrdersList.getDeviceName(), Snackbar.LENGTH_LONG)
//                        .setAction("No action", null).show();
            }
        });
    }
    public void callmyOrdetailsView(MyOrdersList myOrdersList){

        Intent mYOrderDetailsActivity = new Intent(getActivity(), MYOrderDetailsActivity.class);
        mYOrderDetailsActivity.putExtra("listValueDetails", myOrdersList);
        mYOrderDetailsActivity.putExtra("orderStatus", statusList);
        startActivity(mYOrderDetailsActivity);

    }
}
