# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth import authenticate
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import TemplateView # Import TemplateView
from rest_framework import viewsets, status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from rest_framework.decorators import api_view
from models import *
from serializers import *
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from time import time
from Crypto.Cipher import AES
from django.contrib.auth.hashers import make_password, check_password, is_password_usable
import random
import smtplib
import urllib
import datetime
import json
secret_key = 'etouchvirtusaapk' # using for encode and decode password
cipher = AES.new(secret_key,AES.MODE_ECB) # never use ECB in strong systems obviously

# Create your views here.

class SmptLib():

    def send_mail(self, you, subject, body):
        me = "pythonappiumtest@gmail.com"
        you = you
        # Create message container - the correct MIME type is multipart/alternative.
        msg = MIMEMultipart('alternative')
        msg['Subject'] = subject
        msg['From'] = me
        msg['To'] = you
        text = body
        part1 = MIMEText(text, 'plain')
        msg.attach(part1)
        mail = smtplib.SMTP('smtp.gmail.com:587')
        mail.ehlo()
        mail.starttls()
        mail.login('pythonappiumtest@gmail.com', 'eTouch@123')
        mail.sendmail(me, you, msg.as_string())
        mail.quit()

def time_random():
	return time() - float(str(time()).split('.')[0])

def gen_random_range(min, max):
	return int(time_random() * (max - min) + min)

@api_view(['GET', 'POST'])
def snippet_list(request):
    """
    List all code snippets, or create a new snippet.
    """
    renderer_classes = (JSONRenderer, )

    if request.method == 'GET':
        snippets = Snippet.objects.all()
        serializer = SnippetSerializer(snippets, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
	request.data['access_code'] = gen_random_range(100000, 999999)
	request.data['password'] = make_password(request.data['password'])
        serializer = SnippetSerializer(data=request.data)
        if serializer.is_valid():
            """serializer.save()"""
	    response = json.loads(serializer.validated_data)
	    if response['message'] == "Registered Successfully":
		    obj = SmptLib()
		    mail = response['userdetails']['email']
		    subject = "Verification with Etouch!"
		    body = '''
			Hello {first_name}!

			This message has been sent to you because you entered your e-mail on a Etouch CheckinApp.
			If wasn't you, please ignore this mail.

			Please copy this code and paste it into your app to complete this email verification process.

			Your verification code: {number}


			Thanks!
			Team
			'''.format(first_name=response['userdetails']['first_name'], number=response['userdetails']['access_code'])
    		    obj.send_mail(mail, subject, body)
		    response = {'status_code': status.HTTP_200_OK,'message': response['message'], 'userdetails': response['userdetails']}
	    else:
	            response = {'status_code': status.HTTP_200_OK,'message': response['message']}
	    return Response(response)
	else:
		response = {'status_code': status.HTTP_400_BAD_REQUEST,'message': serializer.errors}
        	return Response(response)


@api_view(['GET', 'POST'])
def verifyotp(request):
    """
    List all code snippets, or create a new snippet.
    """
    renderer_classes = (JSONRenderer, )

    if request.method == 'GET':
        snippets = Snippet.objects.all()
        serializer = OtpSerializer(snippets, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = OtpSerializer(data=request.data)
	if serializer.is_valid():
		response = {'status_code': status.HTTP_200_OK,'message': serializer.validated_data}
		return Response(response)
	else:
		response = {'status_code': status.HTTP_400_BAD_REQUEST,'message': serializer.errors}
		return Response(response)




@api_view(['GET', 'POST'])
def login(request):
    """
    List all code snippets, or create a new snippet.
    """
    renderer_classes = (JSONRenderer, )

    if request.method == 'GET':
        snippets = Snippet.objects.all()
        serializer = LoginSerializer(snippets, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
	request.data['password'] = base64.b64encode(cipher.encrypt(request.data['password'].rjust(32)))
        serializer = LoginSerializer(data=request.data)
	if serializer.is_valid():		
		response = json.loads(serializer.validated_data)
	   	if response['message'] == "Login Success":
			response['userdetails']['password'] = cipher.decrypt(base64.b64decode(response['userdetails']['password']))
			if response['userdetails']['password'].strip() == 'etouch@123':
				response = {'status_code': status.HTTP_200_OK,'message': response['message'], 'userid': response['userdetails']['id'], 'email': response['userdetails']['email'], 'deviceid': response['userdetails']['deviceid'], 'password': response['userdetails']['password'].strip(), 'userstatus': '1'}
			else:
				response = {'status_code': status.HTTP_200_OK,'message': response['message'], 'userid': response['userdetails']['id'], 'email': response['userdetails']['email'], 'deviceid': response['userdetails']['deviceid'], 'password': response['userdetails']['password'].strip(), 'userstatus': '0'}
		else:
			response = {'status_code': status.HTTP_400_BAD_REQUEST,'message': response['message']}
		return Response(response)
	else:
		response = {'status_code': status.HTTP_400_BAD_REQUEST,'message': serializer.errors}
		return Response(response)



@api_view(['GET', 'POST'])
def checkin(request):
    """
    List all code snippets, or create a new snippet.
    """
    renderer_classes = (JSONRenderer, )
    serializer = LogSerializer(data=request.data)
    if serializer.is_valid():
	    response = json.loads(serializer.validated_data)
	    if response['message'] == 'success':
		    response = {'status_code': status.HTTP_200_OK,'message': response['message'], 'userdetails': response['userdetails']}
	    else:
		    response = {'status_code': status.HTTP_400_BAD_REQUEST,'message': response['message'], 'userdetails': ""}
    else:
	    checkkey = json.loads(json.dumps(serializer.errors))
	    if checkkey.has_key("userid"):
		message = "userid is required"
	    elif checkkey.has_key("deviceid"):
		message = "Deviceid is required"
	    else:
		message = ""
	    response = {'status_code': status.HTTP_400_BAD_REQUEST,'message': message}
    return Response(response)



@api_view(['GET', 'POST'])
def changepassword(request):
    """
    List all code snippets, or create a new snippet.
    """
    renderer_classes = (JSONRenderer, )
    serializer = Changepassword(data=request.data)
    if serializer.is_valid():
	response = json.loads(serializer.validated_data)
	response = {'status_code': response['status'], 'message': response['message']}
    else:
	response = {'status_code': status.HTTP_400_BAD_REQUEST,'message': serializer.errors}
    return Response(response)



@api_view(['GET', 'POST'])
def projects(request):
    renderer_classes = (JSONRenderer, )    
    projects = ProjectSerializer(Projects.objects.all(), many=True)
    count = Projects.objects.all().count()    
    result = json.dumps(projects.data)
    if count > 0:
	response = {'status_code': status.HTTP_200_OK,'message': "success", "projects": json.loads(result)}
    else:
	response = {'status_code': status.HTTP_400_BAD_REQUEST,'message': "No Projects Found"}	
    return Response(response)


@api_view(['GET', 'POST'])
def postorder(request):
    """
    create a new device order
    """
    renderer_classes = (JSONRenderer, )
    serializer = PostorderSerializer(data=request.data)
    if serializer.is_valid():
	response = json.loads(serializer.validated_data)
	if response['message'] == 'success':
		response = {'status_code': status.HTTP_200_OK,'message': response['message']}
	else:
		response = {'status_code': status.HTTP_400_BAD_REQUEST,'message': response['message']}
    else:	   
	    response = {'status_code': status.HTTP_400_BAD_REQUEST,'message': serializer.errors}
    return Response(response)


@api_view(['GET', 'POST'])
def myorders(request):
    userid = request.query_params.get('userid')
    renderer_classes = (JSONRenderer, )    
    orders = MyordersSerializer(Orders.objects.filter(userid=userid).order_by('-id'), many=True)  
    count = Orders.objects.filter(userid=userid).count()  
    for row in orders.data:
	projectDetails = Projects.objects.get(id=row['project'])
	row['project_name'] = projectDetails.project_name
	if row['status'] != 'Yet to checkout':
		row['update_status'] = '1'
	else:
		row['update_status'] = '0'
    result = json.dumps(orders.data)
    if count > 0:
	orderStatus = dict()
	orderStatus = {'Yet to checkout','In Transit','OOS','In Hand'}
	response = {'status_code': status.HTTP_200_OK,'message': "success", "orders": json.loads(result), "order_status": orderStatus}
    else:
	response = {'status_code': status.HTTP_400_BAD_REQUEST,'message': "No Orders Found"}	
    return Response(response)
    

@api_view(['GET', 'POST'])
def updateorder(request):
   
    renderer_classes = (JSONRenderer, )
    serializer = UpdateorderSerializer(data=request.data)
    if serializer.is_valid():
	response = json.loads(serializer.validated_data)
	if response['message'] == 'Order updated successfully':
		response = {'status_code': status.HTTP_200_OK, 'message': response['message']}
	else:
		response = {'status_code': status.HTTP_400_BAD_REQUEST, 'message': response['message']}
    else:
	response = {'status_code': status.HTTP_400_BAD_REQUEST, 'message': serializer.errors}
    return Response(response)


"""--------------------Web Views Start From Here--------------------------"""

def Loginpage(request):
    if "userid" not in request.session:
	return render(request,'login.html')
    else:
	response = redirect('/dashboard/')
    	return response
	

def logout(request):
    if "userid" in request.session:
    	del request.session['userid']
    	response = redirect('/')
    	return response
    else:
	return redirect('/')


@api_view(['GET', 'POST'])
def Checkdetails(request):
   email = request.query_params.get('email')
   password = request.query_params.get('password')
   password = base64.b64encode(cipher.encrypt(password.rjust(32)))
   checkUser = Users.objects.filter(email=email,password=password)
   if checkUser.exists():
	usercondition = Users.objects.get(email=email)
	userdetails = dict();
	request.session['userid'] = usercondition.id
	response = {'status_code': status.HTTP_200_OK, 'message': 'Login Success'}
   else:
	response = {'status_code': status.HTTP_400_BAD_REQUEST, 'message': 'Invalid Details'}
   return Response(response)


def Dashboard(request):
	if "userid" in request.session:   
		userid = request.session['userid']
		orders = Orders.objects.filter(userid=userid).order_by('-id')
		userorders  = {'orders':orders}		
		return render(request,'dashboard.html', userorders)
	else:
		response = redirect('/')
		return response
    

def Createorder(request):
    if "userid" in request.session:
	projects = Projects.objects.all()
	listofprojects  = {'projects':projects}	
	return render(request,'postorder.html', listofprojects)
    else:
	return redirect('/')


def Orderdetail(request, pk):
     if "userid" in request.session:
	orderdetails = Orders.objects.get(id=pk)
	return render(request,'orderdetails.html', {'orderdetails': orderdetails})
     else:
	return redirect('/')


