from django.contrib.auth.models import User, Group
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import serializers
from django.db import models
from models import *
from django.template.context_processors import csrf
import datetime
from django.db.models import Q
from rest_framework.validators import UniqueTogetherValidator
from django.http import JsonResponse
import json
import base64
from django.contrib.auth.hashers import make_password, check_password, is_password_usable
from Crypto.Cipher import AES
secret_key = 'etouchvirtusaapk' # using for encode and decode password
cipher = AES.new(secret_key,AES.MODE_ECB) # never use ECB in strong systems obviously
currentdate = datetime.datetime.now()
todaydatetime = currentdate.strftime('%Y-%m-%d %H:%M:%S')

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')



class ExampleSerializer(serializers.Serializer):
    # ...
    class Meta:
        # ToDo items belong to a parent list, and have an ordering defined
        # by the 'position' field. No two items in a given list may share
        # the same position.
        validators = [
            UniqueTogetherValidator(
                queryset=Snippet.objects.all(),
                fields=('email')
            )
        ]

class SnippetSerializer(serializers.Serializer):
    first_name = serializers.CharField(required=True)
    last_name = serializers.CharField(required=True)
    email = serializers.CharField(required=True)
    contact = serializers.CharField(required=True)
    password = serializers.CharField(required=True)
    created_date = serializers.CharField(required=False)
    access_code = serializers.CharField(required=False)
    status = serializers.CharField(required=False)


    def validate(self, validated_data):
	email = validated_data['email']
	try:
		userdetails = dict()
		userDetails = Snippet.objects.filter(email=email)
		if userDetails.exists():
			response = {"message":"Email Already Exists"}
			return json.dumps(response)
		else:
			"""print validated_data"""
			userdata = Snippet.objects.create(**validated_data)
			userdata.save()
			userdetails['id'] = userdata.id
			userdetails['email'] = userdata.email
			userdetails['access_code'] = userdata.access_code
			userdetails['first_name'] = userdata.first_name
			userdetails['password'] = userdata.password
			response = {"message":"Registered Successfully", "userdetails": userdetails}
			return json.dumps(response)
	except KeyError:
			raise serializers.ValidationError("Something went wrong")



class OtpSerializer(serializers.Serializer):
    email = serializers.CharField(required=True)
    access_code = serializers.CharField(required=True)

    def validate(self, data):
	email = data['email']
	access_code = data['access_code']
	try:
		response = dict()
		userDetails = Snippet.objects.filter(email=email,access_code=access_code)
		if userDetails.exists():
			Snippet.objects.filter(email=email).update(status = 1)
			response = "OTP verified success"
		else:
			response = "Invalid OTP"
		return response
	except KeyError:
		raise serializers.ValidationError("Something went wrong")



class LoginSerializer(serializers.Serializer):
    email = serializers.CharField(required=True)
    password = serializers.CharField(required=False)
    deviceid = serializers.CharField(required=True)
    login_type = serializers.CharField(required=True)

    def validate(self, validated_data):
	email = validated_data['email']
	password = validated_data['password']
	deviceid = validated_data['deviceid']
	login_type = validated_data['login_type']
	try:
		response = dict()	
		userDetails = Users.objects.filter(email=email)
		if userDetails.exists():
			if login_type == 'virtusa': 
				checkUser = Users.objects.filter(email=email,password=password)
				if checkUser.exists():
					usercondition = Users.objects.get(email=email,password=password)
					if cipher.decrypt(base64.b64decode(usercondition.password)).strip() == 'etouch@123':
						Users.objects.filter(email=email).update(deviceid = deviceid)
					checkdevice = Users.objects.filter(email=email,password=password,deviceid=deviceid)
					if checkdevice.exists():
						userStatus = Users.objects.filter(email=email,password=password,status='1')
						if userStatus.exists():			
							usercondition = Users.objects.get(email=email,password=password)
							userdetails = dict();
							userdetails['id'] = usercondition.id
							userdetails['deviceid'] = usercondition.deviceid
							userdetails['email'] = usercondition.email
							userdetails['password'] = usercondition.password                 			
							response = {"message": "Login Success", "userdetails": userdetails}
						else:
							response = {"message": "You are in inactive state please contact admin"}
					else:
						response = {"message": "You are already logged-in with other device, Please contact admin to reset your account"}
				else:
					response = {"message": "Invalid Credentials"}
			else:
				usercondition = Users.objects.get(email=email)
				if usercondition.deviceid == "":
					Users.objects.filter(email=email).update(deviceid = deviceid)
				checkUser = Users.objects.filter(email=email,deviceid = deviceid)
				if checkUser.exists():						
					usercondition = Users.objects.get(email=email)
					userdetails = dict();
					userdetails['id'] = usercondition.id
					userdetails['deviceid'] = usercondition.deviceid
					userdetails['email'] = usercondition.email
					userdetails['password'] = usercondition.password                 			
					response = {"message": "Login Success", "userdetails": userdetails}
				else:
					response = {"message": "You are already logged-in with other device, Please contact admin to reset your account"}
		else:
			response = {"message": "No account exists with this email id"}
		return json.dumps(response)
	except KeyError:
		raise serializers.ValidationError("Something went wrong")


class Createuser(serializers.Serializer):
    email = serializers.CharField(required=True)
    password = serializers.CharField(required=True)
    status = serializers.CharField(required=False)
    deviceid = serializers.CharField(required=True)
    created_date = serializers.DateField(required=False)

    def create(self, validated_data):
        """
        Create and return a new `Snippet` instance, given the validated data.
        """
	userdata = Users.objects.create(**validated_data)
	userdata.save()
	response = {"message":"Registered Successfully", "userdetails": userdata}
	return json.dumps(response)


class LogSerializer(serializers.Serializer):
    userid = serializers.CharField(required=True)
    email = serializers.CharField(required=True)
    lattitude = serializers.CharField(required=True)
    longitude = serializers.CharField(required=True)
    deviceid = serializers.CharField(required=True)
    date_time = serializers.DateTimeField(required=False)

    def validate(self, validated_data):
	email = validated_data['email']
	deviceid = validated_data['deviceid']
	try:
		userdetails = dict()
		checkEmail = Users.objects.filter(email=email)
		if checkEmail.exists():
			userDetails = Users.objects.filter(email=email,deviceid=deviceid)
			if userDetails.exists():
				alreadycheckin = Userlog.objects.filter(email=email,date=datetime.date.today())
				if alreadycheckin.exists():
					response = {"message":"Data already exists"}
				else:					
					userdata = Userlog.objects.create(**validated_data)
					userdata.save()
					userdetails['date_time'] = userdata.date_time
					userdetails['lattitude'] = userdata.lattitude
					userdetails['longitude'] = userdata.longitude
					response = {"message":"success", "userdetails": userdetails}
				return json.dumps(response)
			else:
				response = {"message":"You are logged in other device,please contact admin"}
				return json.dumps(response)
		else:
			response = {"message":"No account exists with this emailid"}
			return json.dumps(response)
	except KeyError:
			raise serializers.ValidationError("Something went wrong")


class Changepassword(serializers.Serializer):
    email = serializers.CharField(required=True)
    oldpassword = serializers.CharField(required=True)
    newpassword = serializers.CharField(required=True)

    def validate(self, data):
	email = data['email']
	oldpassword = data['oldpassword']
	newpassword = data['newpassword']
	try:
		response = dict()
		userDetails = Users.objects.filter(email=email)
		if userDetails.exists():
			checkoldpassword = Users.objects.filter(email=email,password=base64.b64encode(cipher.encrypt(oldpassword.rjust(32))))
			if checkoldpassword.exists():
				Users.objects.filter(email=email).update(password = base64.b64encode(cipher.encrypt(newpassword.rjust(32))))
				if base64.b64encode(cipher.encrypt(oldpassword.rjust(32))) == base64.b64encode(cipher.encrypt(newpassword.rjust(32))):
					response = {"status": status.HTTP_400_BAD_REQUEST, "message": "New password should not be same as old password"}
				else:
					response = {"status": status.HTTP_200_OK, "message":"Password updated successfully"}
			else:
				response = {"status": status.HTTP_400_BAD_REQUEST, "message":"Incorrect old password"}
		else:
			response = {"status": status.HTTP_400_BAD_REQUEST, "message":"Invalid email id"}
		return json.dumps(response)
	except KeyError:
		raise serializers.ValidationError("Something went wrong")


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Projects
        fields = ('id', 'project_name', 'project_code')



class PostorderSerializer(serializers.Serializer):
    userid = serializers.IntegerField(required=True)
    project_id = serializers.IntegerField(required=True)
    srno = serializers.CharField(required=True)
    inventory_size = serializers.CharField(required=True)
    device_name = serializers.CharField(required=True)
    device_market_link = serializers.CharField(required=True)

    def validate(self, validated_data):
        """
        Create and return a new `Snippet` instance, given the validated data.
        """
	orderdata = Orders.objects.create(**validated_data)
	orderdata.save()	
	response = {"message":"success"}
	return json.dumps(response)


class MyordersSerializer(serializers.ModelSerializer):
   
    class Meta:
        model = Orders
        fields = ('id', 'userid', 'project', 'srno', 'inventory_size', 'device_name', 'device_market_link', 'requested_date', 'received_date', 'status')


class UpdateorderSerializer(serializers.Serializer):
   orderid = serializers.IntegerField(required=True)
   status = serializers.CharField(required=True)

   def validate(self, validated_data):
	orderid = validated_data['orderid']
        status = validated_data['status']
	try:
		response = dict()
		orderDetails = Orders.objects.filter(id=orderid)
		if orderDetails.exists():
			Orders.objects.filter(id=orderid).update(status=status,received_date=currentdate)
			response = {"message": "Order updated successfully"}
		else:
			response = {"message":"Invalid order id"}
		return json.dumps(response)
	except KeyError:
		raise serializers.ValidationError("Something went wrong")
