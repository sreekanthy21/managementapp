# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
import datetime
import random
from time import time
# Create your models here.


# class Person(models.Model):
#     name = models.CharField(max_length=30)
#     email = models.CharField(max_length=50)
#     # latitude = models.DecimalField(max_digits=15)
#     # longitude = models.DecimalField(max_digits=15)

todaydate = datetime.datetime.now()

def time_random():
	return time() - float(str(time()).split('.')[0])

def gen_random_range(min, max):
	return int(time_random() * (max - min) + min) 

class Snippet(models.Model):
    random_number = gen_random_range(100000, 999999)
    todaydate = datetime.datetime.now()
    first_name = models.CharField(max_length=25)
    last_name = models.CharField(max_length=25)
    email = models.CharField(max_length=50, unique=True)
    contact = models.CharField(max_length=15)
    password = models.CharField(max_length=50, default='etouch@123')
    created_date = models.DateField(default=todaydate)
    access_code = models.CharField(max_length=10)
    status = models.IntegerField(default=0)

    class Meta:
        ordering = ('__created',)


class Users(models.Model):
    todaydate = datetime.datetime.now()
    todaydatetime = todaydate.strftime('%Y-%m-%d')
    email = models.CharField(max_length=50, unique=True)
    role_id = models.CharField(max_length=25, default='NOT NULL')
    password = models.CharField(max_length=50, default='etouch@123')    
    status = models.IntegerField(default=0)
    deviceid = models.CharField(max_length=10)
    created_date = models.DateField(default=todaydatetime)

    class Meta:
        ordering = ('__created',)



class Userlog(models.Model):
    x = datetime.datetime.now()
    todaydatetime = x.strftime('%Y-%m-%d %H:%M:%S')

    userid = models.IntegerField()
    email = models.CharField(max_length=50, default="NOT NULL")
    lattitude = models.CharField(max_length=50)
    longitude = models.CharField(max_length=50)
    deviceid = models.CharField(max_length=245, default="NULL")
    date_time = models.DateTimeField(default=todaydatetime)
    date = models.DateField(default=datetime.date.today())

    class Meta:
        ordering = ('__created',)


class Projects(models.Model):
	id = models.AutoField(primary_key=True)
	project_name = models.CharField(max_length=500, default="NULL")
	project_code = models.CharField(max_length=500, default="NULL")
	project_description = models.TextField()
	execution_type = models.CharField(max_length=245, default="NULL")
	area_project = models.CharField(max_length=245, default="NULL")
 	
	class Meta:
		managed = True
		db_table = 'projects'


class Orders(models.Model):
	id = models.AutoField(primary_key=True)
	userid = models.IntegerField(blank=False, null=False)
	project = models.ForeignKey(Projects)
	srno = models.CharField(max_length=245, default="NOT NULL")
	inventory_size = models.CharField(max_length=245, default="NULL")
	device_name = models.CharField(max_length=245, default="NULL")
	device_market_link = models.CharField(max_length=245, default="NULL")
	requested_date = models.DateField(default=todaydate)
	received_date = models.DateField(null=True)
	status = models.CharField(max_length=245, default="Yet to checkout")

	class Meta:
		managed = True
		db_table = 'orders'




