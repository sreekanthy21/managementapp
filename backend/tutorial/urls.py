"""tutorial URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import url, include
from rest_framework import routers
from quickstart import views


urlpatterns = [
    url(r'^register/', views.snippet_list),
    url(r'^checkin/', views.checkin),
    url(r'^verifyotp/', views.verifyotp),
    url(r'^login/', views.login),
    url(r'^changepassword/', views.changepassword),
    url(r'^projects/', views.projects),
    url(r'^postorder/', views.postorder),
    url(r'^myorders/', views.myorders),
    url(r'^updateorder/', views.updateorder),
    url(r'^$', views.Loginpage, name='weblogin'), # Web Url's start from here
    url(r'^checkdetails/', views.Checkdetails, name='checkdetails'),
    url(r'^dashboard/', views.Dashboard, name='dashboard'),
    url(r'^createorder/', views.Createorder, name='createorder'),
    url(r'orderdetails/(?P<pk>[-\w]+)', views.Orderdetail, name='orderdetails'),
    url(r'^logout/', views.logout, name='logout')
]
